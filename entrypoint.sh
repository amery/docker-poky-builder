#!/bin/sh

set -eu

# find root of the workspace
find_repo_workspace_root() {
	if [ -d "$1/.repo" ]; then
		echo "$1"
	elif [ "$1" != / ]; then
		find_repo_workspace_root "$(dirname "$1")"
	fi
}

WS=$(find_repo_workspace_root "$WORKSPACE")

# OEROOT
OEROOT=
for x in \
	${WS:+"$WS"} \
	${WS:+"$WS/sources"} \
	${BUILDDIR:+"$BUILDDIR/.."} \
	${BUILDDIR:+"$BUILDDIR/../sources"} \
	"$WORKSPACE" \
	"$WORKSPACE/sources" \
	"$WORKSPACE/.." \
	"$WORKSPACE/../sources" \
	; do

	for y in /poky ""; do
		if [ -s "$x$y/oe-init-build-env" ]; then
			OEROOT="$(cd "$x$y" && pwd)"
			break 2
		fi
	done
done

if [ -z "$OEROOT" ]; then
	echo "Invalid workspace, poky/oe-init-build-env not found." >&2
	exit 2
elif [ -z "$WS" ]; then
	case "$OEROOT" in
	*/sources/poky)
		WS="${OEROOT%/sources/poky}"
		;;
	*/poky)
		WS="${OEROOT%/poky}"
		;;
	*)
		echo "failed to detect workspace root" >&2
		exit 2
	esac
fi

if [ -z "${USER_HOME}" ]; then
	USER_HOME="/home/$USER_NAME"
fi

# create workspace-friendly user
groupadd -r -g "$USER_GID" "$USER_NAME"
useradd -r -g "$USER_GID" -u "$USER_UID" \
	-s /bin/bash -d "$USER_HOME" "$USER_NAME"

if [ ! -s "$USER_HOME/.profile" ]; then
	for x in $(find /etc/skel ! -type d); do
		y="$USER_HOME/${x##/etc/skel/}"
		mkdir -p "${y%/*}"
		cp -a "$x" "$y"
		chown "$USER_NAME:$USER_NAME" "$y"
	done
	chown "$USER_NAME:$USER_NAME" "$USER_HOME"
fi

# downloads directory
#
if [ -z "${DL_DIR:-}" ]; then
	DL_DIR="$WS/downloads"
fi

if [ -L "$DL_DIR" ]; then
	DL_DIR="$(readlink -f "$DL_DIR")"
fi

if [ ! -d "$DL_DIR/" ]; then
	mkdir "$DL_DIR"
	chown "$USER_NAME:$USER_NAME" "$DL_DIR"
fi

BB_ENV_EXTRAWHITE="${BB_ENV_EXTRAWHITE:+$BB_ENV_EXTRAWHITE }DL_DIR"

# builddir
#
if [ -x "$WS/setup-environment" ]; then
	build_env="setup-environment"
else
	build_env="${OEROOT#$WS/}/oe-init-build-env"
fi

if [ -n "${BUILDDIR:-}" -a "${BUILDDIR:-}" != "$WS" ]; then
	builddir="${BUILDDIR#$WS/}"
else
	builddir=build${MACHINE:+-$MACHINE}${DISTRO:+-$DISTRO}${TCLIBC:+-$TCLIBC}
fi

# call command, if any
#
if [ $# -gt 0 ]; then
	bb_cmd="bitbake $*"
	cmd="$*"

	case "$1" in
	./*|/*)
		;;
	-*)
		cmd="$bb_cmd"
		;;
	*)
		if [ -x "`which "$1"`" ]; then
			:
		elif [ -f "$OEROOT/bitbake/bin/$1" -a \
		       -x "$OEROOT/bitbake/bin/$1" ]; then
			:
		elif [ -f "$OEROOT/scripts/$1" -a \
		       -x "$OEROOT/scripts/$1" ]; then
			:
		else
			cmd="$bb_cmd"
		fi
	esac
else
	cmd=
fi

F=/etc/profile.d/Z99-docker-run.sh

cat <<EOT > "$F"
cd '$WS'

EOT

# environment
#
for x in DL_DIR MACHINE DISTRO TCLIBC BB_ENV_EXTRAWHITE; do
	v="$(eval echo "\${$x:-}")"
	if [ -n "$v" ]; then
		echo "export $x='$v'"
	fi
done >> "$F"

cat <<EOT >> "$F"

OEROOT="$OEROOT" source $build_env $builddir
EOT

# jump to the old $PWD if needed
#
if [ -d "$WORKSPACE" -a "$WS" != "$WORKSPACE" -a "${BUILDDIR:-}" != "$WORKSPACE" ]; then
	echo "cd '$WORKSPACE'" >> $F
fi

if [ -n "$cmd" ]; then
	echo "exec $cmd" >> $F
fi

grep -n ^ "$F"
set -x
su - "$USER_NAME"
