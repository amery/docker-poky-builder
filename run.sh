#!/bin/sh
set -eu

# select image
DOCKER_DIR="$(dirname "$(readlink -f "$0")")"

# preserve user identity
USER_NAME=$(id -urn)
USER_UID=$(id -ur)
USER_GID=$(id -gr)

# docker image
docker build --rm "$DOCKER_DIR"
DOCKER_ID="$(docker build --rm -q "$DOCKER_DIR" | tail -n1)"

set -- \
	-e USER_HOME="$HOME" \
	-e USER_NAME=${USER_NAME} \
	-e USER_UID=${USER_UID} \
	-e USER_GID=${USER_GID} \
	${MACHINE:+-e "MACHINE=$MACHINE"} \
	${DISTRO:+-e  "DISTRO=$DISTRO"} \
	${TCLIBC:+-e  "TCLIBC=$TCLIBC"} \
	"${DOCKER_ID##* }" "$@"

OEROOT=
BUILDDIR=

# find root of the workspace
find_repo_workspace_root() {
	if [ -d "$1/.repo" ]; then
		echo "$1"
	elif [ "$1" != / ]; then
		find_repo_workspace_root "$(dirname "$1")"
	fi
}
WS="$(find_repo_workspace_root "$PWD")"

if [ -s conf/local.conf ]; then
	BUILDDIR="$PWD"
	set -- -e BUILDDIR="$BUILDDIR" "$@"
fi

# OEROOT
for x in \
	${WS:+"$WS"} \
	${WS:+"$WS/sources"} \
	${BUILDDIR:+"$BUILDDIR"/..} \
	${BUILDDIR:+"$BUILDDIR"/../sources} \
	"$PWD" "$PWD/sources" \
	"$PWD/.." "$PWD/../sources" \
	; do

	for y in /poky ""; do
		if [ -s "$x$y/oe-init-build-env" ]; then
			OEROOT="$(cd "$x$y" && pwd)"
			break 2
		fi
	done
done

if [ -z "$OEROOT" ]; then
	echo "Invalid workspace, poky/oe-init-build-env not found." >&2
	exit 2
fi

# persistent volumes
cache_dir="${WS:-$OEROOT}/.docker-run-cache/cache"
home_dir="${WS:-$OEROOT}/.docker-run-cache/home/$USER_NAME"

for x in "$cache_dir" "$home_dir"; do
	[ -d "$x" ] || mkdir -p "$x"
done

# support symbolic references to parent and sibbling directories
parent_dir="$(dirname "$PWD")"

# repo init --reference
REPO_REF_DIR=
if [ -n "$WS" -a -s "$WS/.repo/manifests.git/config" ]; then
	x="$(git config --file "$WS/.repo/manifests.git/config" repo.reference || echo)"
	if [ -n "$x" -a -d "$x" ]; then
		REPO_REF_DIR="$(cd "$x" && pwd)"
	fi
fi

# downloads/
if [ -z "${DL_DIR:-}" ]; then
	if [ -n "$WS" ]; then
		DL_DIR="$WS/downloads"
	else
		case "$OEROOT" in
		*/sources/poky)
			DL_DIR=${OEROOT%/sources/poky}
			;;
		*/poky)
			DL_DIR="${OEROOT%/poky}"
			;;
		*)
			DL_DIR="$OEROOT"
			;;
		esac
		DL_DIR="$DL_DIR/downloads"
	fi
fi

if [ -L "$DL_DIR" ]; then
	DL_DIR="$(readlink -f "$DL_DIR")"
fi

[ -d "$DL_DIR/" ] || mkdir "$DL_DIR"

# environment variables
set -- \
	-e DL_DIR="$DL_DIR" \
	-e WORKSPACE="$PWD" \
	"$@"

# bind volumes uniquely and in the right order
volumes() {
	local x=
	sort -uV | while read x; do
		case "$x" in
		"$HOME")
			echo "-v $home_dir:$x"
			;;
		/cache)
			echo "-v $cache_dir:$x"
			;;
		'')
			;;
		*)
			echo "-v $x:$x"
			;;
		esac
	done
}

set -- $(volumes <<EOT
/cache
$parent_dir
$HOME
$PWD
$BUILDDIR
$DL_DIR
$REPO_REF_DIR
EOT
) "$@"

# and finally run within the container
set -x
exec docker run --rm -ti "$@"
