This image tags itself using the name of the parent directory, to get `hanover/docker-morty-builder`
we need to check it out at `~/projects/docker/hanover/docker-morty-builder/`

```sh
mkdir -p ~/projects/docker/hanover
cd ~/projects/docker/hanover
git clone  http://git.hanover.local/docker/docker-morty-builder.git
cd docker-morty-builder
./build.sh
ln -snf $PWD/run.sh /usr/local/bin/morty-docker-run
```

then we use it as bitbake wrapper as `morty-docker-run` followed by `bitbake` arguments
```sh
mkdir ~/projects/poky-2.2
cd ~/projects/poky-2.2
git clone -b morty git://git.yoctoproject.org/poky.git
morty-docker-run core-image-sato
```
